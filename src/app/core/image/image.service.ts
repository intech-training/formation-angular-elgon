import { Injectable } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

@Injectable()
export class ImageService {

  constructor(private sanitizer: DomSanitizer) { }

  getImageUrl(source: string): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${source})`);
  }

}
