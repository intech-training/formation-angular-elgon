import { Card } from './../../shared/models/card';
import { TestBed, async, fakeAsync, tick } from '@angular/core/testing';
import { Injectable } from '@angular/core';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import { CardService } from './card.service';
import { Observable, timer } from 'rxjs';
import { map, mapTo } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

describe('CardService', () => {
  let service: CardService;
  let httpMock: HttpTestingController;

  const mockCards: Card[] = [
    {
      hp: 40,
      id: '1',
      imageUrl: 'https://images.pokemontcg.io/base1/58.png',
      imageUrlHiRes: 'https://images.pokemontcg.io/base1/58_hires.png',
      name: 'Test',
      nationalPokedexNumber: 1,
      number: 1,
      rarity: 'Common',
      subtype: 'Basic',
      supertype: 'Test',
      types: ['Fire']
    },
    {
      hp: 120,
      id: '2',
      imageUrl: 'https://images.pokemontcg.io/base1/57.png',
      imageUrlHiRes: 'https://images.pokemontcg.io/base1/57_hires.png',
      name: 'Other Test',
      nationalPokedexNumber: 2,
      number: 2,
      rarity: 'Common',
      subtype: 'Basic',
      supertype: 'Test',
      types: ['Leaf']
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CardService]
    });
    service = TestBed.get(CardService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => httpMock.verify());

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return an Observable<Card[]>', () => {
    service.getCards().subscribe(cards => {
      expect(cards.length).toBe(2);
      expect(cards).toEqual(mockCards);
    });

    const req = httpMock.expectOne(`${environment.apiUrl}/cards`);
    expect(req.request.method).toBe('GET');
    // On exécute manuellement la requête HTTP afin de finir le test
    req.flush(mockCards);
  });
});

@Injectable()
class ValueService {
  getValue(): string {
    return '42';
  }
}
@Injectable()
class MasterService {
  getValue(): string {
    return '42';
  }
}

@Injectable()
class WeatherService {
  getTemperature(city: string): Observable<number> {
    return timer(1000).pipe(
      mapTo(city),
      map(_ => 20)
    );
  }
}

// describe('ValueService', () => {
//   let valueServiceSpy: jasmine.SpyObj<ValueService>;
//   let masterService: MasterService;

//   beforeEach(() => {
//     const spy = jasmine.createSpyObj('ValueService', ['getValue']);

//     TestBed.configureTestingModule({
//       providers: [
//         MasterService,
//         { provide: ValueService, useValue: spy }
//       ]
//     });
//     valueServiceSpy = TestBed.get(ValueService);
//     masterService = TestBed.get(MasterService);
//   });

//   it('should return 42', () => {
//     const stubValue = 'stub-value';
//     valueServiceSpy.getValue.and.returnValue(stubValue);

//     expect(masterService.getValue()).toBe(stubValue);
//     expect(valueServiceSpy.getValue.calls.count()).toBe(1, 'spy method was called once');
//     expect(valueServiceSpy.getValue.calls.mostRecent().returnValue).toBe(stubValue);
//   });
// });

describe('WeatherService', () => {

  beforeEach(() => TestBed.configureTestingModule({providers: [WeatherService]}));

  it('should return the right temperature', fakeAsync(() => {
    const weatherService: WeatherService = TestBed.get(WeatherService);

    let temperature;

    weatherService.getTemperature('Luxembourg')
      .subscribe(_temperature => {
        temperature = _temperature;
      });

    tick(1000);

    expect(temperature).toEqual(20);

  }));
});


