import { CoreModule } from './../core.module';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Card } from '../../shared/models/card';
import { environment } from 'src/environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: CoreModule
})
export class CardService {

  constructor(private http: HttpClient) { }

  getCards(): Observable<Card[]> {
    return this.http.get<Card[]>(`${environment.apiUrl}/cards`)
      .pipe(map(response => {
        return response['cards'];
      }));
  }

  getCard(id: string): Observable<Card> {
    return this.http.get<Card>(`${environment.apiUrl}/cards/${id}`);
  }
}
