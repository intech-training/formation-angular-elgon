import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { NgModule, Optional, SkipSelf } from '@angular/core';

import { AuthGuardService } from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';
import { CardService } from './card/card.service';
import { httpInterceptorProviders } from './http-interceptors';
import { RequestCacheWithMap } from './http-interceptors/request-cache.service';
import { ImageService } from './image/image.service';
import { DownloadFileUtilsService } from './download-file-utils/download-file-utils.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,

    HttpClientModule
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        RequestCacheWithMap,
        httpInterceptorProviders,
        AuthService,
        ImageService,
        CardService,

        AuthGuardService
      ]
    };
  }
}
