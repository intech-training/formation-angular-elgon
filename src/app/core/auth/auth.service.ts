import { Injectable } from '@angular/core';


@Injectable()
export class AuthService {

  private FAKE_TOKEN = '1234';

  constructor() { }

  getToken(): string {

    return this.FAKE_TOKEN;
  }

  isLoggedIn(): boolean {
    return true;
  }

}
