import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {FileWithFilename} from './fileWithFilename.model';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';


@Injectable()
export class DownloadFileUtilsService {

  constructor(private http: HttpClient ) { }

  public getBinaryWithFilename(url: string): Observable<FileWithFilename> {

    return  this.http.get(url, { responseType : 'blob', observe : 'response' })
      .pipe(map((httpResponse: HttpResponse<Blob>) => {
        const contentDisposition = httpResponse.headers.get('Content-Disposition');


        const fileName = <string>((contentDisposition).replace('attachment; filename=', '')).trim();
        return {
          blob      : httpResponse.body,
          filename  : fileName
        };
      }));
  }

  public downloadBinaryFromBlob(dataBlob: Blob, filename: string, mimeType: string = 'application/octet-stream') {

    const blob = new Blob([dataBlob], {type: mimeType});
    const url = window.URL.createObjectURL(blob);

    const a: HTMLAnchorElement = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = filename;
    a.click();
    window.URL.revokeObjectURL(url);
  }

}
