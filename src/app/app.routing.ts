import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

const routes: Routes = [
  { path: 'cards', loadChildren: './cards/cards.module#CardsModule' },
  { path: '',  redirectTo: 'cards', pathMatch: 'full' },
  { path: '**', redirectTo: 'not-found-error' }
];

// Les options du router qui nous permettent de configurer le routing
// - enableTracing : pour logger tous les événements internes dans la console
// - initialNavigation : désactive la navigation initiale
// - errorHandler : permet de définir un handler pour les erreurs du router, pour voir un comportement différent de celui d'Angular par défaut
// - preloadingStrategy : Angular peut utiliser le lazy loading pour charger tous les composants et services de l'application. Cela permet d'être plus rapide au chargement.
//    - PreloadAllModules : pour activer cette fonctionnalité de preloading
//    - Il est également possible d'y indiquer sa propre stratégie de preloading (décider de ce qui doit être préchargé ou non)
//    - par défaut, le preloading n'est pas activé
// - onSameUrlNavigation : définit ce que doit faire le router lorsque l'on navigue sur l'URL courante. par défaut, le router ignore.
//    - reload : recharge la page lorsque l'on navigue sur l'URL courante
//    - ignore : défaut
// - scrollPositionRestoration : https://angular.io/api/router/ExtraOptions#scrollPositionRestoration
// - anchorScrolling : https://angular.io/api/router/ExtraOptions#anchorScrolling
// - scrollOffset : https://angular.io/api/router/ExtraOptions#scrollOffset
// - paramsInheritanceStrategy :
//     - emptyOnly (défaut): les paramètres et data ne sont pas hérités des routes parents, sauf s'il s'agit d'une route sans composant ou path
//     - always : les paramètres et data seront hérités des routes parents dans tous les cas
// - malformedUriErrorHandler : permet d'indiquer un handler d'erreur pour les erreur d'URI malformées.
// - urlUpdateStrategy : deferred | eager. Pour définir à quel moment le router va mettre à jour l'url du browser. Le comportement par défaut est de mettre à jour l'URL quand une navigation est en success.
// - relativeLinkResolution : legacy | corrected : https://angular.io/api/router/ExtraOptions#relativeLinkResolution
// - useHash : définit le mode de gestion du routeur : avec le hash (pour les anciens navigateurs ne supportant pas HTML5), ou sans (false par défaut)
const routingConfiguration: ExtraOptions = {
  // paramsInheritanceStrategy: 'always',
  // useHash : true
};

@NgModule({
  imports: [RouterModule.forRoot(routes, routingConfiguration)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
