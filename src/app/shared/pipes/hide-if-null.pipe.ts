import {Pipe, PipeTransform} from '@angular/core';

import * as _ from 'lodash';

@Pipe({ name: 'hideIfNull'})
export class HideIfNullPipe implements PipeTransform {

  constructor() {}

  public transform(value: any): any {
    return (_.isNil(value)) ? '' : value;
  }
}
