import { HideIfNullPipe } from './pipes/hide-if-null.pipe';
import { HideIfNullDirective } from './directives/hide-if-null.directive';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HighlightDirective } from './directives/highlight.directive';

@NgModule({
  declarations: [TopBarComponent, HideIfNullDirective, HideIfNullPipe, HighlightDirective],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, MaterialModule],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MaterialModule,

    TopBarComponent,

    HideIfNullDirective,
    HideIfNullPipe,
    HighlightDirective
  ]
})
export class SharedModule {}
