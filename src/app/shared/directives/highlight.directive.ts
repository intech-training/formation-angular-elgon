import { Directive, ElementRef, Input, HostListener } from '@angular/core';

/**
 * EXERCICE 02
 * Création d'une directive permettant de changer la couleur de fond de l'élément au survol.
 * Attention : la couleur de fond doit revenir à sa couleur d'origine après le survol
 * Vous pouvez créer des variables de classes
 */
@Directive({
  selector: '[cardHighlight]'
})
export class HighlightDirective {

  @Input() highlightColor: string;

  @HostListener('mouseenter') onMouseEnter() {
    // TODO: To implement
  }

  @HostListener('mouseleave') onMouseLeave() {
    // TODO: To implement
  }

  constructor(private el: ElementRef) {}

  private highlight(color: string) {
    // TODO: To implement
  }

  private savePreviousColor(): void {
    // TODO: To implement
  }
}
