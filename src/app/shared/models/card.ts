export interface Card {
  id: string;
  name: string;
  nationalPokedexNumber: number;
  imageUrl: string;
  imageUrlHiRes: string;
  types: string[];
  hp: number;
  number: number;
  subtype: string;
  supertype: string;
  rarity: string;
}
