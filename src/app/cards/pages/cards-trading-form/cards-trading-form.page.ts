import { CardService } from '../../../core/card/card.service';
import { Trading } from '../../../shared/models/trading';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl, FormArray } from '@angular/forms';
import * as _ from 'lodash';
import * as moment from 'moment';
import { map, first } from 'rxjs/operators';

@Component({
  selector: 'app-cards-trading-form',
  templateUrl: './cards-trading-form.page.html',
  styleUrls: ['./cards-trading-form.page.scss']
})
export class CardsTradingFormPage implements OnInit {
  public trading: Trading = {
    card1: '',
    card2: ''
  };
  public tradingForm: FormGroup;
  public card1Control: FormControl;
  public card2Control: FormControl;
  public dateControl: FormControl;
  public genderControl: FormControl;
  public nameControl: FormControl;

  public addressFormArray: FormArray;
  public streetControl: FormControl;
  public postalCodeControl: FormControl;

  static isDateValid(control: FormControl) {
    const momentDate = moment(control.value);

    if (!momentDate.isValid) {
      return { invalidDate: true };
    }

    return momentDate.isBefore(moment()) ? { invalidDate: true } : null;
  }

  constructor(private formBuilder: FormBuilder,
              private cardService: CardService) { }

  ngOnInit() {
    // On crée le formulaire
    this.initForm();
  }

  handleForm(): void {
    // On gère la soumission du formulaire
    console.log(this.tradingForm.value);
  }

  private initForm(): void {
    // this.tradingForm = this.formBuilder.group({
    //   card1: this.formBuilder.control('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]),
    //   card2: this.formBuilder.control('', Validators.required)
    // });

    this.card1Control = this.formBuilder.control('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]);
    this.card2Control = this.formBuilder.control('', Validators.required, [control => this.isCardValid(control)]);
    this.dateControl = this.formBuilder.control(null, [Validators.required, CardsTradingFormPage.isDateValid]);
    this.nameControl = this.formBuilder.control('', [Validators.required]);
    this.genderControl = this.formBuilder.control(null, [Validators.required]);

    this.streetControl = this.formBuilder.control('', Validators.required);
    this.postalCodeControl = this.formBuilder.control('');

    this.tradingForm = this.formBuilder.group({
      card1: this.card1Control,
      card2: this.card2Control,
      date: this.dateControl,
      gender: this.genderControl,
      name: this.nameControl,
      address: this.formBuilder.array([ this.initAddress() ])
    });
  }

  private initAddress(): FormGroup {
    this.streetControl = this.formBuilder.control('', Validators.required);
    this.postalCodeControl = this.formBuilder.control('');

    return this.formBuilder.group({
        street: this.streetControl,
        postalCode: this.postalCodeControl
      });
  }

  private isCardValid(control: AbstractControl) {
    const cardName = control.value;
    return this.cardService.getCards().pipe(
      map(cards => _.find(cards, { name: cardName }) ? null : {'invalidCard': true}),
      first()
    );
  }

}
