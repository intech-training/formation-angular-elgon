import { FormGroup, FormArray, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-cards-trading-address-field',
  templateUrl: './cards-trading-address-field.component.html',
  styles: [`
  .trading-input {
    width: 100%;
  }
  `]
})
export class CardsTradingAddressFieldComponent implements OnInit {
  @Input() public parentForm: FormGroup;

  public streetControl: FormControl;
  public postalControl: FormControl;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.streetControl = this.formBuilder.control('', Validators.required);
    this.postalControl = this.formBuilder.control('');

    this.parentForm.addControl('address', this.formBuilder.array([
      this.formBuilder.group({
        street: this.streetControl,
        postalCode: this.postalControl
      })
    ]));
  }

}
