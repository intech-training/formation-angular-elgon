import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from './../../../core/core.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsTradingFormPage } from './cards-trading-form.page';
import { SharedModule } from 'src/app/shared/shared.module';

describe('CardsTradingFormComponent', () => {
  let component: CardsTradingFormPage;
  let fixture: ComponentFixture<CardsTradingFormPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, CoreModule, BrowserAnimationsModule],
      declarations: [ CardsTradingFormPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsTradingFormPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
