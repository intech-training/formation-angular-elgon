import { CardService } from '../../../core/card/card.service';
import { SafeStyle } from '@angular/platform-browser';
import { Card } from '../../../shared/models/card';
import { Component, OnInit, Inject } from '@angular/core';
import { ImageService } from 'src/app/core/image/image.service';
import { ActivatedRoute, Params, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-cards-detail',
  templateUrl: './cards-detail.page.html',
  styleUrls: ['./cards-detail.page.scss']
})
export class CardsDetailPage implements OnInit {
  public card: Card;
  public cardObservable: Observable<Card>;

  constructor(private imageService: ImageService,
              private cardService: CardService,
              private route: ActivatedRoute) { }



  ngOnInit() {
    this.route.params.pipe(
      map((params: Params) => params['id']),
      switchMap(id => this.cardService.getCard(id))
    ).subscribe((card: Card) => this.card = card);

  }

  getImage(source: string): SafeStyle {
    return this.imageService.getImageUrl(source);
  }

}
