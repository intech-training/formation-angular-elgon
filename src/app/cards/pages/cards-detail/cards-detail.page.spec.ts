import { Card } from './../../../shared/models/card';
import { CardService } from './../../../core/card/card.service';
import { CoreModule } from './../../../core/core.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsDetailPage } from './cards-detail.page';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';

describe('CardsDetailComponent', () => {
  let component: CardsDetailPage;
  let fixture: ComponentFixture<CardsDetailPage>;

  const mockCard: Card = {
    hp: 40,
    id: '1',
    imageUrl: 'https://images.pokemontcg.io/base1/58.png',
    imageUrlHiRes: 'https://images.pokemontcg.io/base1/58_hires.png',
    name: 'Test',
    nationalPokedexNumber: 1,
    number: 1,
    rarity: 'Common',
    subtype: 'Basic',
    supertype: 'Test',
    types: ['Fire']
  };

  beforeEach(async(() => {
    const cardServiceSpy = jasmine.createSpyObj('CardService', ['getCard']);
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, SharedModule, CoreModule ],
      declarations: [ CardsDetailPage ],
      providers: [
        { provide: CardService, useValue: cardServiceSpy }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
