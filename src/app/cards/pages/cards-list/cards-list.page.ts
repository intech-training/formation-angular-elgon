import { CardService } from '../../../core/card/card.service';
import { Card } from '../../../shared/models/card';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import {DownloadFileUtilsService} from '../../../core/download-file-utils/download-file-utils.service';
import {environment} from '../../../../environments/environment';

import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';

@Component({
  selector: 'app-cards-list',
  templateUrl: './cards-list.page.html',
  styleUrls: ['./cards-list.page.scss']
})
export class CardsListPage implements OnInit, OnDestroy {
  public cards: Card[] = [];
  // Multiple layout for switch
  public layouts: string[] = ['Card List', 'Table'];
  // Set default layout
  public currentLayout: string = this.layouts[0];

  public set sortOrder(value: 'asc' | 'desc') {
    if (value) {
      // On assigne la valeur récupérée dans un query param de l'URL
      this.router.navigate(['/cards'], {
        queryParams: {
          sort: value
        }
      });
      // Puis on trie les cartes selon cet ordre
      this.sortCards(value);
    }
  }
  public get sortOrder(): 'asc' | 'desc' {
    return <'asc' | 'desc' > this.route.snapshot.queryParamMap.get('sort');
  }

  private cardsSubscription: Subscription;
  private queryParamsSubscription: Subscription;

  constructor(private cardService: CardService,
    private downloadFileUtils: DownloadFileUtilsService,
    private router: Router,
    private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.loadAllCards();
  }

  ngOnDestroy(): void {
    if (this.cardsSubscription) {
      this.cardsSubscription.unsubscribe();
    }
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
  }

  loadAllCards(): void {
    this.cardsSubscription = this.cardService.getCards().subscribe(cards => {
      this.cards = cards;

      this.queryParamsSubscription = this.route.queryParams
        .pipe(map(params => params.order))
        .subscribe(order => (this.sortOrder = order));
    });
    this.cardsSubscription = this.cardService.getCards().subscribe(cards => {
      this.cards = cards;
      if (this.sortOrder) {
        this.sortCards(this.sortOrder);
      }
    });
  }

  downloadRulebook(): void {

    this.downloadFileUtils
      .getBinaryWithFilename(`${environment.apiUrl}/rulebook.pdf`)
      .pipe(
        map(fileWithFilename => {
          this.downloadFileUtils.downloadBinaryFromBlob(fileWithFilename.blob, fileWithFilename.filename);
        })
      )
      .subscribe();
  }

  openCardDetails(cardClicked: Card): void {
    this.router.navigate(['/cards', cardClicked.id], { queryParams: { order: 'asc' } });
  }

  private sortCards(order: 'asc' | 'desc'): void {
    this.cards = _.orderBy(this.cards, ['name'], [order]);
  }
}
