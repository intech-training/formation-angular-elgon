import { CardSummaryComponent } from './../../components/card-summary/card-summary.component';
import { Observable, Subject } from 'rxjs';
import { Card } from './../../../shared/models/card';
import { CardService } from './../../../core/card/card.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from './../../../core/core.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsListPage } from './cards-list.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { NO_ERRORS_SCHEMA, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';

describe('CardsListPage', () => {
  let component: CardsListPage;
  let fixture: ComponentFixture<CardsListPage>;

  const mockCards: Card[] = [
    {
      hp: 40,
      id: '1',
      imageUrl: 'https://images.pokemontcg.io/base1/58.png',
      imageUrlHiRes: 'https://images.pokemontcg.io/base1/58_hires.png',
      name: 'Test',
      nationalPokedexNumber: 1,
      number: 1,
      rarity: 'Common',
      subtype: 'Basic',
      supertype: 'Test',
      types: ['Fire']
    },
    {
      hp: 120,
      id: '2',
      imageUrl: 'https://images.pokemontcg.io/base1/57.png',
      imageUrlHiRes: 'https://images.pokemontcg.io/base1/57_hires.png',
      name: 'Other Test',
      nationalPokedexNumber: 2,
      number: 2,
      rarity: 'Common',
      subtype: 'Basic',
      supertype: 'Test',
      types: ['Leaf']
    }
  ];
  const mockRoute = { snapshot: { queryParamMap: { get: () => undefined }}};
  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    mockRoute['queryParams'] = new Subject<any>();

    const routes = [{ path: 'cards', component: CardsListPage }];

    TestBed.configureTestingModule({
      imports: [ SharedModule, CoreModule, RouterTestingModule.withRoutes(routes), NoopAnimationsModule ],
      declarations: [ CardsListPage, CardSummaryComponent ],
      providers: [
        {
          provide: CardService,
          useValue: {
            getCards: () => new Observable(observer => observer.next(mockCards))
          }
        },
        { provide: ActivatedRoute, useValue: mockRoute },
        { provide: Router, useValue: routerSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsListPage);
    component = fixture.componentInstance;
    // trigger the change detection
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title "Cards list" in h2 tag', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('h2').textContent).toContain('Cards list');
  });

  it('should have an array of cards with the same length as mock', () => {
    expect(component.cards).toBeDefined();
    expect(component.cards.length).toEqual(mockCards.length);
  });

  it('should order the cards in when the URL query param changes', () => {
    // On teste d'abord si l'état initial des cartes est ok (non ordonnée)
    expect(component.cards).toEqual(mockCards);

    // On définit ce que l'on attend comme résultat (les cartes triées selon leur nom)
    let orderedCardsExpected = _.cloneDeep(mockCards);
    orderedCardsExpected = _.orderBy(orderedCardsExpected, ['name'], 'asc');

    // On change le query param à asc
    mockRoute['queryParams'].next({ order: 'asc'});
    fixture.detectChanges();

    // On vérifie que les cartes du composant se sont bien triées
    expect(component.cards).toEqual(orderedCardsExpected);

    // On fait pareil, mais avec desc
    orderedCardsExpected = _.orderBy(orderedCardsExpected, ['name'], 'desc');

    // On change le query param à desc
    mockRoute['queryParams'].next({ order: 'desc'});
    fixture.detectChanges();

    // On vérifie si cela correspond à ce qu'on attend
    expect(component.cards).toEqual(orderedCardsExpected);
  });

  it('should call router.navigate when click on a card', () => {
    component.openCardDetails(mockCards[0]);
    const navArgs = routerSpy.navigate.calls.mostRecent().args[0];

    expect(navArgs).toEqual(['/cards', mockCards[0].id]);
  });


});
