import { RouterModule } from '@angular/router';
import { AboutPage } from './pages/about/about.page';
import { CardsRoutingModule } from './cards.routing';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { CardsListPage } from './pages/cards-list/cards-list.page';
import { CardsDetailPage } from './pages/cards-detail/cards-detail.page';
import { CardsTradingFormPage } from './pages/cards-trading-form/cards-trading-form.page';
import { CardsComponent } from './cards.component';
import { CardSummaryComponent } from './components/card-summary/card-summary.component';
import { CardsTradingAddressFieldComponent } from './pages/cards-trading-form/components/cards-trading-address-field/cards-trading-address-field.component';

@NgModule({
  declarations: [
    CardsListPage,
    CardsDetailPage,
    CardsTradingFormPage,
    CardsComponent,
    AboutPage,
    CardSummaryComponent,
    CardsTradingAddressFieldComponent
  ],
  imports: [
    CommonModule,
    CardsRoutingModule,
    RouterModule,

    SharedModule
  ],
  entryComponents: [
    CardsDetailPage
  ]
})
export class CardsModule { }
