import { AuthGuardService } from './../core/auth/auth-guard.service';
import { AboutPage } from './pages/about/about.page';
import { CardsTradingFormPage } from './pages/cards-trading-form/cards-trading-form.page';
import { NgModule } from '@angular/core';
import { CardsDetailPage } from './pages/cards-detail/cards-detail.page';
import { CardsListPage } from './pages/cards-list/cards-list.page';
import { Routes, RouterModule } from '@angular/router';
import { CardsComponent } from './cards.component';

const routes: Routes = [
  {
    path: '', component: CardsComponent, canActivate: [AuthGuardService],
    children: [
      { path: '', component: CardsListPage },
      { path: ':id', component: CardsDetailPage },
      { path: 'trading/new', component: CardsTradingFormPage }
    ]
  },
  { path: 'info/about', component: AboutPage }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardsRoutingModule {
}
