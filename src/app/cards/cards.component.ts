import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html'
})
export class CardsComponent implements OnInit {
  public navLinks: { path: any[], label: string }[];

  constructor() { }

  ngOnInit() {
    this.navLinks = [
      { path: ['/cards'], label: 'List' },
      { path: ['/cards', 'trading', 'new'], label: 'New trading'},
      { path: ['/cards', 'info', 'about'], label: 'About' }
    ];
  }

}
