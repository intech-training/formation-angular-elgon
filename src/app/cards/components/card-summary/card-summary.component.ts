import { Card } from './../../../shared/models/card';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ImageService } from 'src/app/core/image/image.service';
import { SafeStyle } from '@angular/platform-browser';

@Component({
  selector: 'app-card-summary',
  templateUrl: './card-summary.component.html',
  styleUrls: ['./card-summary.component.scss']
})
export class CardSummaryComponent {
  @Input() card: Card;
  @Output() cardClicked: EventEmitter<Card> = new EventEmitter<Card>();

  constructor(private imageService: ImageService) { }

  public getCardImageStyle(source: string): SafeStyle {
    return this.imageService.getImageUrl(source);
  }

  public onCardClick(): void {
    this.cardClicked.emit(this.card);
  }

}
