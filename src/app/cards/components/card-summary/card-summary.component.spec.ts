import { Card } from './../../../shared/models/card';
import { ImageService } from 'src/app/core/image/image.service';
import { SafeStyle } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardSummaryComponent } from './card-summary.component';
import { SharedModule } from 'src/app/shared/shared.module';

describe('CardSummaryComponent', () => {
  let component: CardSummaryComponent;
  let fixture: ComponentFixture<CardSummaryComponent>;

  const mockCard: Card = {
    hp: 40,
    id: '1',
    imageUrl: 'https://images.pokemontcg.io/base1/58.png',
    imageUrlHiRes: 'https://images.pokemontcg.io/base1/58_hires.png',
    name: 'Test',
    nationalPokedexNumber: 1,
    number: 1,
    rarity: 'Common',
    subtype: 'Basic',
    supertype: 'Test',
    types: ['Fire']
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ CardSummaryComponent ],
      providers: [ ImageService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardSummaryComponent);
    component = fixture.componentInstance;
    // On crée une carte pour le composant
    component.card = mockCard;
    fixture.detectChanges();
  });

  it('should emit an event cardClicked', () => {
    // On ajoute un espion à la méthode 'emit' de notre EventEmitter
    spyOn(component.cardClicked, 'emit');

    // On simule un click sur la carte
    const element = fixture.nativeElement;
    const cardElement = element.querySelector('mat-card');
    cardElement.dispatchEvent(new Event('click'));

    // On force une détection des changements
    fixture.detectChanges();

    // Enfin, on teste si l'event emitter a bien lancé notre événement
    expect(component.cardClicked.emit).toHaveBeenCalled();
  });

  it('should have a h2 tag with the name of the card', () => {
    const element = fixture.nativeElement;
    expect(element.querySelector('h2').textContent).toContain(mockCard.name);
  });
});
